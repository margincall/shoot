/* global PIXI, screen, flyable */

var shooting = (function() {

    var player = null;
    var enemies = null;

    var playerBullets = null;
    var enemyBullets = null;

    var keyState = {
        left: false,
        up: false,
        right: false,
        down: false,
        space: false
    };

    var bomb = {
        nBombLeft: 0,
        bombLeftText: undefined,
        explode: function() {
            if (this.nBombLeft <= 0 ) return;

            this.nBombLeft--;
            this.bombLeftText.text = this.nBombLeft;

            for (var i=0; i<600; i+=10) {
                for (var j=0; j<10; j++) {
                    var bullet = new flyable.PlayerBullet(screen.stage, {x:i, y:800});
                    playerBullets.push(bullet);
                }
            }
        }
    };

    var scenario = {
        queue: [],
        init: function() {
            // create a texture from an image path
            //var bgTexture = PIXI.Texture.fromImage('girls_guiding.png');
           var bgTexture = PIXI.Texture.fromImage('girls.jpg'); //for test

            this.bg = new PIXI.Sprite(bgTexture);
            this.bg.anchor.x = 0;
            this.bg.anchor.y = 0;
            this.bg.position.x = 0;
            this.bg.position.y = -800;
            this.bg.scale.x += screen.BG_PLUS_SIZE;
            this.bg.scale.y += screen.BG_PLUS_SIZE;
            this.bg.speedX = 0;
            this.bg.speedY = 0;
            screen.stage.addChild(this.bg);

            playerBullets = new flyable.FlyableList(screen.stage);
            playerBullets.name = 'playerBullets';
            player = new flyable.Player(screen.stage, playerBullets);
            enemyBullets = new flyable.FlyableList(screen.stage);
            enemyBullets.name = 'enemyBullets';
            enemies = new flyable.FlyableList(screen.stage);

            var breakableTexture = PIXI.BaseTexture.fromImage('girls_breakable.png'); //800, 600
            this.breakables = [];
            var i=0,j=0;
            var partTexture = null;
            var breakableSizeX = 600;
            var breakableSizeY = 800;
            this.nPartX = 20;
            this.nPartY = 20;
            this.partX = breakableSizeX/this.nPartX; //"2*" hardcoded for small girls image
            this.partY = breakableSizeY/this.nPartY;

            for (i = 0; i < this.nPartX; i++) {
                for (j = 0; j < this.nPartY; j++) {
                    if (i===1 && j===0) continue;
                    partTexture = new PIXI.Texture(
                        breakableTexture,new PIXI.Rectangle(i*this.partX,j*this.partY,this.partX,this.partY));
                    //partTexture = PIXI.Texture.fromImage('cat.jpg');
                    var breakable = new PIXI.Sprite(partTexture);
                    breakable.anchor.x = 0;
                    breakable.anchor.y = 0;
                    breakable.position.x = i*this.partX;
                    breakable.position.y = j*this.partY;
                    this.breakables.push(breakable);
                    this.bg.addChild(breakable);
                }
            }

            this.breakableXNumber = 1 + screen.BG_PLUS_SIZE;

            //set Bomb
            bomb.nBombLeft = 3;
            var bombButton = new PIXI.Sprite(PIXI.Texture.fromImage('bomb.png'));
            bombButton.position.x = 20;
            bombButton.position.y = 700;
            screen.stage.addChild(bombButton);

            var bombLeftText = new PIXI.Text(bomb.nBombLeft, {font : '30px Arial', fill : 0x000000, align : 'center'});
            bombLeftText.position.x = 90;
            bombLeftText.position.y = 720;
            bomb.bombLeftText = bombLeftText;
            screen.stage.addChild(bombLeftText);

            // stage 1
            this.queue.push([0, this.setBgSpeed, -3, 1]);
            this.queue.push([150, this.spawnEnemy, 350, -20]);
            this.queue.push([300, this.spawnEnemy, 250, -20]);
            this.queue.push([600, this.spawnEnemy, 400, -20]);
            this.queue.push([600, this.spawnEnemy, 200, -20]);
            this.queue.push([700, this.spawnLineAttackEnemy, 150, -20]);
            this.queue.push([800, this.setBgSpeed, 3, 1]);
            this.queue.push([900, this.spawnEnemy, 200, -20]);
            this.queue.push([1000, this.spawnEnemy, 400, -30]);
            this.queue.push([1200, this.spawnEnemy, 200, -30]);
            this.queue.push([1600, this.setBgSpeed, -3, 1]);
            this.queue.push([1800, this.spawnEnemy, 100, -20]);
            this.queue.push([1800, this.spawnEnemy, 400, -20]);
            this.queue.push([2400, this.setBgSpeed, 3, 1]);
            this.queue.push([2600, this.spawnEnemy, 400, -10]);
            this.queue.push([2600, this.spawnEnemy, 200, -10]);
            this.queue.push([2600, this.spawnEnemy, 200, -10]);
            this.queue.push([3400, this.showFullImage, 0, 0]);
        },
        conditionCheck: function(frameCounter) {
            if (this.queue.length > 0)
                return frameCounter >= this.queue[0][0];
        },
        spawnEnemy: function(x, y) {
            enemies.push(new flyable.Enemy(screen.stage, enemyBullets, {x:x, y:y}));
            enemies.last.aim(player);
        },
        spawnLineAttackEnemy: function(x, y) {
            var enemy = new flyable.Enemy(screen.stage, enemyBullets, {x:x, y:y});
            enemy.rotation = Math.PI;
            enemy.aim = function() {};
            enemies.push(enemy);
        },
        setBgSpeed: function(x, y) {
            scenario.bg.speedX = x;
            scenario.bg.speedY = y;
        },
        showFullImage: function(x, y) {
            scenario.bg.speedX = 0;
            scenario.bg.speedY = 0;

            scenario.bg.scale.x = 1;
            scenario.bg.scale.y = 1;
        }
    };

    // TODO: 시나리오에 따라 실행되도록.. method가 실행되기는 하나?
    var animate = (function() {
        var frameCounter = 0;

        //keyState.keys = Object.keys(keyState);
        var DIRECTION = ['left', 'up', 'right', 'down'];

        function bulletLoop(bullets, target) {
            var old;
            var x = 0;
            var y = 0;

            // 총알 처리 : 화면 지나면 제거 & 앞으로 전진 & 적 충돌
            for (var bullet = bullets.first; bullet; bullet = bullet.nextOne) {
                x = bullet.position.x;
                y = bullet.position.y;
                if (y > screen.CANVAS_HEIGHT || y < 0) {
                    bullets.remove(old, bullet);
                    continue;
                }

                if (bullets.name === 'playerBullets') {
                    var isRemoved = false;
                    //enemy 죽을 때 메모리 회수 안 되고 있음.
                    for (var enemy = enemies.first; enemy; enemy = enemy.nextOne) {
                        // collision check
                        if (enemy.isAlive) {
                            if (x >= enemy.position.x-enemy.halfSize && x <= enemy.position.x+enemy.halfSize &&
                                y >= enemy.position.y-enemy.halfSize && y <= enemy.position.y+enemy.halfSize) {
                                    // 총알 제거
                                    bullets.remove(old, bullet);
                                    isRemoved = true;

                                    enemy.getDamaged(1); //TODO: hardcoded damage
                            }
                        }
                    }

                    if (isRemoved) {
                        continue;
                    }

                    //breakable 충돌 체크
                    var breakable = null;
                    var len = scenario.breakables.length;
                    for (var i = 0; i < len; i++) {
                        breakable = scenario.breakables[i];
                        if (breakable.visible === false) continue;
                        var positionX = scenario.breakableXNumber * breakable.position.x + scenario.bg.position.x;
                        var positionY = scenario.breakableXNumber * breakable.position.y + scenario.bg.position.y;
                        if (x >= positionX && x <= positionX + scenario.partX*2 &&
                            y >= positionY && y <= positionY + scenario.partY*2) {
                                bullets.remove(old, bullet);
                                isRemoved = true;
                                breakable.visible = false;
                        }
                    }

                    if (isRemoved) {
                        continue;
                    }

                    // block collision loop
                } else {
                    // collision check
                    if (x >= player.position.x-player.halfSize && x <= player.position.x+player.halfSize &&
                        y >= player.position.y-player.halfSize && y <= player.position.y+player.halfSize) {
                            // 총알 제거
                            enemyBullets.remove(old, bullet);
                            continue;
                            //alert('game over');
                    }
                }

                // move forward
                bullet.move();
                old = bullet;
            }
        }

        return function() {
            requestAnimationFrame(animate);

            var enemy;

            if (scenario.conditionCheck(frameCounter)) {
                var event = scenario.queue.shift();
                event[1](event[2], event[3]);
            }

            //player move
            for (var i=0; i<DIRECTION.length; i++) {
                if (keyState[DIRECTION[i]]) {
                    player.move(DIRECTION[i]);
                    for (enemy = enemies.first; enemy; enemy = enemy.nextOne) {
                        enemy.aim(player);
                    }
                }
            }

            // add one bullet & bg moves up / 4 frames
            if (frameCounter%4 === 0) {
                if (keyState.space) player.shoot();
                for (enemy = enemies.first; enemy; enemy = enemy.nextOne) {
                    enemy.move();
                    if (enemy.isAlive && frameCounter%60 === 0) enemy.shoot();
                }
                if (scenario.bg.position.y < 0) {
                    scenario.bg.position.x += scenario.bg.speedX;
                    scenario.bg.position.y += scenario.bg.speedY;
                }
            }

            // 총알 처리 : 화면 지나면 제거 & 앞으로 전진 & 충돌
            bulletLoop(playerBullets);
            bulletLoop(enemyBullets);

            frameCounter++;
            setTimeout( function() {
                $('#frame_counter').html(frameCounter);
            }, 50 );

            // render the container
            screen.renderer.render(screen.stage);
        };
    })();

    function init() {
        screen.init();
        scenario.init();

        //keyboard
        $(document).keydown(function(event) {
            // left arrow	37
            // up arrow	    38
            // right arrow	39
            // down arrow	40
            switch(event.which){
                case 37:
                    keyState.left = true;
                    break;
                case 38:
                    keyState.up = true;
                    break;
                case 39:
                    keyState.right = true;
                    break;
                case 40:
                    keyState.down = true;
                    break;
                case 32: // spacebar
                    keyState.space = true;
                    break;
                case 90: // 'z'
                    bomb.explode();
                    break;
            }
        });

        //keyboard
        $(document).keyup(function(event) {
            // left arrow	37
            // up arrow	    38
            // right arrow	39
            // down arrow	40
            switch(event.which){
                case 37:
                    keyState.left = false;
                    break;
                case 38:
                    keyState.up = false;
                    break;
                case 39:
                    keyState.right = false;
                    break;
                case 40:
                    keyState.down = false;
                    break;
                case 32: // spacebar
                    keyState.space = false;
                    break;
            }
        });

        // start animating
        animate();
    }

    return {
        init : init
    };
})();
