/* global PIXI */

var screen = (function() {
    var CANVAS_WIDTH = 600;
    var CANVAS_HEIGHT = 800;
    var BG_PLUS_SIZE = 1;
    
    var renderer = PIXI.autoDetectRenderer(CANVAS_WIDTH, CANVAS_HEIGHT,{backgroundColor : 0x1099bb});
    
    // create the root of the scene graph
    var stage = new PIXI.Container();
    
    var init = function() {
        document.body.appendChild(renderer.view);
    };
    
    return {
        CANVAS_WIDTH : CANVAS_WIDTH,
        CANVAS_HEIGHT : CANVAS_HEIGHT,
        BG_PLUS_SIZE : BG_PLUS_SIZE,
        renderer : renderer,
        stage : stage,
        init : init
    };
})();
