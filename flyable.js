/* global PIXI, screen */

var flyable = (function() {
    // create a texture from an image path
    var playerTexture = PIXI.Texture.fromImage('bunny.png');
    var playerBulletTexture = PIXI.Texture.fromImage('bullet.gif');
    var enemyTexture = PIXI.Texture.fromImage('enemy.gif');

    function Player(stage, bullets) {
        PIXI.Sprite.call(this, playerTexture);

        // center the sprite's anchor point
        this.anchor.x = 0.5;
        this.anchor.y = 0.5;
        this.position.x = 300;
        this.position.y = 600;

        this.speed = 10;
        this.halfSize = 12;

        this.move = function(direction) {
            switch(direction) {
                case "left":
                    if (this.position.x > 0) this.position.x -= this.speed;
                    break;
                case "up":
                    if (this.position.y > 0) this.position.y -= this.speed;
                    break;
                case "right":
                    if (this.position.x < screen.CANVAS_WIDTH) this.position.x += this.speed;
                    break;
                case "down":
                    if (this.position.y < screen.CANVAS_HEIGHT) this.position.y += this.speed;
                    break;
            }
        };
        this.shoot = (function() {
            var bullet = null;

            return function() {
                bullet = new PlayerBullet(stage, this.position);
                bullets.push(bullet);
            };
        })();

        stage.addChild(this);
    }
    Player.prototype = new PIXI.Sprite();
    Player.constructor = Player;
    
    function Enemy(stage, bullets, state) {
        PIXI.Sprite.call(this, enemyTexture);

        // center the sprite's anchor point
        this.anchor.x = 0.5;
        this.anchor.y = 0.5;

        this.position.x = 200;
        this.position.y = 100;
        
        this.hp = 5;
        this.speed = 4;
        this.halfSize = 16;
        this.isAlive = true;
        this.nextOne = undefined;

        if (state) {
            this.position.x = state.x;
            this.position.y = 0 - this.halfSize;
        }

        this.shoot = (function() {
            var bullet = null;
        
            return function() {
                bullet = new EnemyBullet(stage, this.position, this.rotation);
                bullets.push(bullet);
            };
        })();
        this.move = function() {
            if (this.position.y < 300) this.position.y += this.speed;
        };
        this.aim = function(target) {
            this.rotation = Math.PI - Math.atan2(target.position.x - this.position.x, target.position.y - this.position.y);
        };
        this.getDamaged = function(damage) {
            this.hp -= damage;
            if (this.hp <= 0) {
                this.isAlive = false;
                stage.removeChild(this);
            }
        };

        stage.addChild(this);
    }
    Enemy.prototype = new PIXI.Sprite();
    Enemy.constructor = Enemy;

    function PlayerBullet(stage, position) {
        PIXI.Sprite.call(this, playerBulletTexture);

        this.anchor.x = 0.5;
        this.anchor.y = 0.5;
        this.position.x = position.x;
        this.position.y = position.y;

        this.speed = 15;
        this.nextOne = undefined;

        this.move = function() {
            this.position.y -= this.speed;
        };

        stage.addChild(this);
    }
    PlayerBullet.prototype = new PIXI.Sprite();
    PlayerBullet.constructor = PlayerBullet;

    function EnemyBullet(stage, position, angle) {
        PIXI.Sprite.call(this, playerBulletTexture);

        this.anchor.x = 0.5;
        this.anchor.y = 0.5;
        this.position.x = position.x;
        this.position.y = position.y;

        this.speed = {};
        this.speed.x = (5) * Math.sin(angle);
        this.speed.y = (-5) * Math.cos(angle);
        this.nextOne = undefined;

        this.move = function() {
            this.position.x += this.speed.x;
            this.position.y += this.speed.y;
        };

        stage.addChild(this);
    }
    EnemyBullet.prototype = new PIXI.Sprite();
    EnemyBullet.constructor = EnemyBullet;

    function FlyableList(stage) {
        this.first = undefined;
        this.last = undefined;
        this.length = 0;

        this.push = function(item) {
            if (!this.first) this.first = item;
            else this.last.nextOne = item;

            this.last = item;
            this.length++;
        };
        this.remove = function(old, target) {
            if (!old) {
                this.first = target.nextOne;
            } else {
                old.nextOne = target.nextOne;
            }

            if (this.last == target) {
                this.last = old;
            }
            this.length--;

            stage.removeChild(target);
        };
    }

    return {
        Player : Player,
        Enemy : Enemy,
        PlayerBullet : PlayerBullet,
        EnemyBullet : EnemyBullet,
        FlyableList : FlyableList
    };
})();
